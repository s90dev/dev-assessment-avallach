<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Avallac'h</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                margin: 0;
                height: 100%;
                width: 100%;
            }

            .container {
                width: 400px;
                margin: 40px auto;
            }
        </style>

        <link rel="stylesheet" href="{{ mix('/css/app.css') }}">
    </head>
    <body>
        <div class="container" id="app">
            <label>Search for:</label>
            <input type="text"> <br>
            <button type="button">Go!</button>
            <hr>
            <div><pre>@{{ message }}</pre></div>
        </div>
        <script src="{{ mix('/js/app.js') }}"></script>
    </body>
</html>
